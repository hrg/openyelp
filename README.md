openyelp
======
##一个简单的开源评论系统##
前期主要完成和美国yelp一样的功能，后期会增加一些特色功能。
###演示地址:[http://openyelp.sturgeon.mopaas.com/](http://openyelp.sturgeon.mopaas.com/)
账号 ada  密码123456
###spring+springmvc+hibernate+shiro+maven+bootstrap
#####联系方式：2601035599@qq.com
## qq交流群：399591308 ##
#网站首页#
![](http://openyelp.sturgeon.mopaas.com/assets/images/site.png)
# 主要功能 #
- 评论系统
- 聊天系统
- 活动系统
- 推荐系统

# 项目部署 #
1. 修改src/main/resources/jdbc.properties
2. 通过maven运行mvn tomcat7:run
