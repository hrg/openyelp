package com.openyelp.apps;

import java.util.List;

import com.openyelp.data.apps.ObjectFactory;
import com.openyelp.data.entity.EntityContent;
import com.openyelp.data.service.EntityContentService;
import com.openyelp.services.haoservice.lifeservice.joke.ContentList;
import com.openyelp.services.haoservice.lifeservice.joke.JokeApi;
import com.openyelp.services.haoservice.lifeservice.joke.JokeContent;

public class Apps {

	public static void main(String[] args) {
		EntityContentService s = ObjectFactory.get().getBean(
				EntityContentService.class);		ContentList list = JokeApi.ContentList(1, 10);
		List<JokeContent> imgs = list.getResult();
		for (JokeContent jokeContent : imgs) {
			EntityContent e = new EntityContent();
			e.setContent(jokeContent.getContent());
			e.setType(jokeContent.getType());
			e.setTitle(jokeContent.getTitle());
			e.setUpdatetime(jokeContent.getUpdatetime());
			s.save(e);
		}
	}

}
