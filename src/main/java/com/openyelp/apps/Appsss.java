package com.openyelp.apps;

import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

public class Appsss {

	public static void main(String[] args) throws OAuthSystemException {
		// TODO Auto-generated method stub
		OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(
				new MD5Generator());

		String accessToken = oauthIssuerImpl.accessToken();
		String refreshToken = oauthIssuerImpl.refreshToken();

		OAuthResponse r = OAuthASResponse
				.tokenResponse(HttpServletResponse.SC_OK)
				.setAccessToken(accessToken).setExpiresIn("3600")
				.setRefreshToken(refreshToken).buildBodyMessage();
		System.out.println(r.getBody());
		System.out.println(r.getLocationUri());

	}

}
