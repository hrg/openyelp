package com.openyelp.actions.front;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.sd4324530.fastweixin.handle.EventHandle;
import com.github.sd4324530.fastweixin.handle.MessageHandle;
import com.github.sd4324530.fastweixin.message.BaseMsg;
import com.github.sd4324530.fastweixin.message.TextMsg;
import com.github.sd4324530.fastweixin.message.req.TextReqMsg;
import com.github.sd4324530.fastweixin.servlet.WeixinControllerSupport;
import com.github.sd4324530.fastweixin.servlet.WeixinSupport;

@Controller
@RequestMapping(value = "weixin")
public class WeixinController extends WeixinSupport {

	@Override
	protected String getToken() {
		// TODO Auto-generated method stub
		return "happy";
	}

	// 2489250db68ae413d080b5d143d926dc
	@Override
	protected String getAppId() {
		// TODO Auto-generated method stub
		return "wx21d22d5bce9226e4";
	}

	@Override
	protected String getAESKey() {
		// TODO Auto-generated method stub
		return "YmKgTFzDZKbmSnII5MIyAe1Xv9Fk7V1pimOd8LSE4Xk";
	}

	/**
	 * 绑定微信服务器
	 *
	 * @param request
	 *            请求
	 * @return 响应内容
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	protected final String bind(HttpServletRequest request) {
		if (isLegal(request)) {
			// 绑定微信服务器成功
			return request.getParameter("echostr");
		} else {
			// 绑定微信服务器失败
			return "";
		}
	}

	/**
	 * 微信消息交互处理
	 *
	 * @param request
	 *            http 请求对象
	 * @return 响应给微信服务器的消息报文
	 * @throws ServletException
	 *             异常
	 * @throws IOException
	 *             IO异常
	 */

	@RequestMapping(value = "process", method =  RequestMethod.POST)
	@ResponseBody
	public String process(HttpServletRequest request) throws ServletException,
			IOException {
		if (!isLegal(request)) {
			return "";
		}
		return processRequest(request);
	}

	@RequestMapping(value = "process", method = RequestMethod.GET)
	@ResponseBody
	public String processx(HttpServletRequest request) throws ServletException,
			IOException {
		// 微信加密签名
		String signature = request.getParameter("signature");
		// 随机字符串
		String echostr = request.getParameter("echostr");
		// 时间戳
		String timestamp = request.getParameter("timestamp");
		// 随机数
		String nonce = request.getParameter("nonce");
		if (!isLegal(request)) {
			return "happy";
		}
		return echostr;
	}

	// 重写父类方法，处理对应的微信消息
	@Override
	protected BaseMsg handleTextMsg(TextReqMsg msg) {
		String content = msg.getContent();
		// log.debug("用户发送到服务器的内容:{}", content);
		return new TextMsg("服务器回复用户消息!");
	}

	/*
	 * 1.1版本新增，重写父类方法，加入自定义微信消息处理器不是必须的，上面的方法是统一处理所有的文本消息，如果业务觉复杂，上面的会显得比较乱
	 * 这个机制就是为了应对这种情况，每个MessageHandle就是一个业务，只处理指定的那部分消息
	 */
	@Override
	protected List<MessageHandle> initMessageHandles() {
		List<MessageHandle> handles = new ArrayList<MessageHandle>();
		handles.add(new MyMessageHandle());
		return handles;
	}

	// 1.1版本新增，重写父类方法，加入自定义微信事件处理器，同上
	@Override
	protected List<EventHandle> initEventHandles() {
		List<EventHandle> handles = new ArrayList<EventHandle>();
		handles.add(new MyEventHandle());
		return handles;
	}
}
