package com.openyelp.actions.front;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.openyelp.data.apps.ObjectFactory;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.EntityContent;
import com.openyelp.data.service.CommentService;
import com.openyelp.data.service.EntityContentService;
import com.openyelp.services.haoservice.lifeservice.joke.ContentList;
import com.openyelp.services.haoservice.lifeservice.joke.JokeApi;
import com.openyelp.services.haoservice.lifeservice.joke.JokeContent;
import com.openyelp.web.utils.FrontUtils;

@Controller
public class SiteAction {

	@Autowired
	CommentService commentService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		Pagination page = commentService.getPage(curpage, pagesize);
		model.addAttribute("page", page);
		model.addAttribute("list", page.getList());
		model.addAttribute("curpage", curpage);
		model.addAttribute("pagesize", pagesize);
		return FrontUtils.getPath("index");
	}
	@RequestMapping(value = "events", method = RequestMethod.GET)
	public String events(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("events");
	}
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("login");
	}
	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public String signup(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("signup");
	}
	@RequestMapping(value = "joke", method = RequestMethod.GET)
	public String joke(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		EntityContentService s = ObjectFactory.get().getBean(
				EntityContentService.class);		ContentList list = JokeApi.ContentList(curpage, pagesize);
		List<JokeContent> imgs = list.getResult();
		for (JokeContent jokeContent : imgs) {
			EntityContent e = new EntityContent();
			e.setContent(jokeContent.getContent());
			e.setType(jokeContent.getType());
			e.setTitle(jokeContent.getTitle());
			e.setUpdatetime(jokeContent.getUpdatetime());
			s.save(e);
		}
		return "index";
	}
	
	@RequestMapping(value = "login1", method = RequestMethod.GET)
	public String login1(
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return "login";
	}

	@RequestMapping(value = "demo", method = RequestMethod.GET)
	public @ResponseBody String demo(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String result = "demo";
		return result;
	}

	@RequestMapping(value = "downring", method = RequestMethod.GET)
	public String downring(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		try {
			String html = Jsoup
					.connect(
							"http://91mydoor.com/downring/DownRing?type=2&star=0&end=10")
					.execute().body();
			model.addAttribute("msg", html);
		} catch (IOException e) {
			e.printStackTrace();
			model.addAttribute("msg", "erro");
		}

		return "msg";
	}

}
