package com.openyelp.web.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sd4324530.fastweixin.handle.EventHandle;
import com.github.sd4324530.fastweixin.handle.MessageHandle;
import com.github.sd4324530.fastweixin.message.BaseMsg;
import com.github.sd4324530.fastweixin.message.NewsMsg;
import com.github.sd4324530.fastweixin.message.req.BaseEvent;
import com.github.sd4324530.fastweixin.message.req.TextReqMsg;
import com.github.sd4324530.fastweixin.message.req.VoiceReqMsg;
import com.github.sd4324530.fastweixin.servlet.WeixinServletSupport;
import com.openyelp.actions.front.MyEventHandle;
import com.openyelp.actions.front.MyMessageHandle;
import com.openyelp.actions.front.WeixinController;
import com.openyelp.data.apps.ObjectFactory;
import com.openyelp.data.entity.EntityContent;
import com.openyelp.data.service.EntityContentService;
import com.openyelp.services.haoservice.lifeservice.joke.ContentList;
import com.openyelp.services.haoservice.lifeservice.joke.ImgList;
import com.openyelp.services.haoservice.lifeservice.joke.JokeApi;
import com.openyelp.services.haoservice.lifeservice.joke.JokeContent;
import com.openyelp.services.haoservice.lifeservice.joke.JokeImg;

public class WeixinServlet extends WeixinServletSupport {
	private static final Logger log = LoggerFactory
			.getLogger(WeixinController.class);
	private static final String TOKEN = "happy";

	// 设置TOKEN，用于绑定微信服务器
	@Override
	protected String getToken() {
		return TOKEN;
	}

	// 使用安全模式时设置：APPID
	@Override
	protected String getAppId() {
		return null;
	}

	// 使用安全模式时设置：密钥
	@Override
	protected String getAESKey() {
		return null;
	}

	int time = 1;
	int page = 1;

	// 重写父类方法，处理对应的微信消息
	@Override
	protected BaseMsg handleTextMsg(TextReqMsg msg) {
		EntityContentService s = ObjectFactory.get().getBean(
				EntityContentService.class);
		NewsMsg msgg = new NewsMsg();
		String tu = msg.getContent();
		if (time % 10 == 0) {
			ContentList list = JokeApi.ContentList(page, 10);
			List<JokeContent> imgs = list.getResult();
			for (JokeContent jokeContent : imgs) {
				EntityContent e = new EntityContent();
				e.setContent(jokeContent.getContent());
				e.setType(jokeContent.getType());
				e.setTitle(jokeContent.getTitle());
				e.setUpdatetime(jokeContent.getUpdatetime());
				s.save(e);
			}
			page++;
			max = s.all();
		}
		if (tu.indexOf("图") > -1) {
			String content = msg.getContent();
			log.debug("用户发送到服务器的内容:{}", content);
			ImgList list = JokeApi.ImgList(1, 2);
			List<JokeImg> imgs = list.getResult();
			if (imgs != null && imgs.size() > 0) {
				JokeImg img = imgs.get(0);
				msgg.add("" + img.getTitle(), "", "" + img.getUrl(),
						"http://cng1985.sturgeon.mopaas.com/");
			} else {
				msgg.add("信息提示", "明天再来");
			}
		} else {

			int max = s.all();
			if (max < 10) {
				max = 10;
			}
			int x = new Random().nextInt(max);
			EntityContent ee = s.findByRandom(x);
			if (ee != null) {
				msgg.add("" + ee.getTitle(), ee.getContent(), "",
						"http://cng1985.sturgeon.mopaas.com/");
			} else {
				String content = msg.getContent();
				log.debug("用户发送到服务器的内容:{}", content);
				ContentList list = JokeApi.ContentList(1, 10);
				List<JokeContent> imgs = list.getResult();
				for (JokeContent jokeContent : imgs) {
					EntityContent e = new EntityContent();
					e.setContent(jokeContent.getContent());
					e.setType(jokeContent.getType());
					e.setTitle(jokeContent.getTitle());
					e.setUpdatetime(jokeContent.getUpdatetime());
					s.save(e);
				}
				page = 2;
				if (imgs != null && imgs.size() > 0) {
					JokeContent img = imgs.get(0);
					msgg.add("" + img.getTitle(), img.getContent(), "",
							"http://cng1985.sturgeon.mopaas.com/");
				} else {
					msgg.add("信息提示", "明天再来");
				}
			}

		}
		time++;
		return msgg;
	}

	int max = 0;

	protected BaseMsg handleVoiceMsg(VoiceReqMsg msg) {
		NewsMsg msgg = new NewsMsg();

		EntityContentService s = ObjectFactory.get().getBean(
				EntityContentService.class);
		if (max < 1) {
			max = s.all();
		}
		if (max < 10) {
			max = 10;
		}
		int x = new Random().nextInt(max);
		EntityContent ee = s.findByRandom(x);
		if (ee != null) {
			msgg.add("" + ee.getTitle(), ee.getContent(), "",
					"http://cng1985.sturgeon.mopaas.com/");
		} else {
			ContentList list = JokeApi.ContentList(1, 10);
			List<JokeContent> imgs = list.getResult();
			for (JokeContent jokeContent : imgs) {
				EntityContent e = new EntityContent();
				e.setContent(jokeContent.getContent());
				e.setType(jokeContent.getType());
				e.setTitle(jokeContent.getTitle());
				e.setUpdatetime(jokeContent.getUpdatetime());
				s.save(e);
			}
			page = 2;
			if (imgs != null && imgs.size() > 0) {
				JokeContent img = imgs.get(0);
				msgg.add("" + img.getTitle(), img.getContent(), "",
						"http://cng1985.sturgeon.mopaas.com/");
			} else {
				msgg.add("信息提示", "明天再来");
			}
		}
		return msgg;
	}

	/**
	 * 处理添加关注事件，有需要时子类重写
	 *
	 * @param event
	 *            添加关注事件对象
	 * @return 响应消息对象
	 */
	protected BaseMsg handleSubscribe(BaseEvent event) {

		NewsMsg msgg = new NewsMsg();
		msgg.add("欢迎光临", "这里有笑话，有你想不到的惊喜", "",
				"http://cng1985.sturgeon.mopaas.com/");
		return msgg;
	}

	// 1.1版本新增，重写父类方法，加入自定义微信消息处理器
	protected List<MessageHandle> initMessageHandles() {
		List<MessageHandle> handles = new ArrayList<MessageHandle>();
		handles.add(new MyMessageHandle());
		return handles;
	}

	// 1.1版本新增，重写父类方法，加入自定义微信事件处理器
	protected List<EventHandle> initEventHandles() {
		List<EventHandle> handles = new ArrayList<EventHandle>();
		handles.add(new MyEventHandle());
		return handles;
	}
}