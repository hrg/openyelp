package com.openyelp.web.interceptor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.openyelp.web.utils.FrontUtils;

public class SiteHandlerInterceptor extends HandlerInterceptorAdapter {

	private Logger logger = LoggerFactory.getLogger("ada");

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler)
			throws ServletException {
		logger.info("LogHandlerInterceptor  preHandle");
		return true;
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if(modelAndView!=null){
			logger.info("LogHandlerInterceptor  postHandle "
					+ modelAndView.getViewName());
			String siteurl = "siteurl";
			if(url!=null){
				siteurl=url;
			}else{
				siteurl="http://"+request.getLocalAddr()+":"+request.getLocalPort()+request.getContextPath()+"/";
			}
			logger.info("url:"+ url);
			modelAndView.addObject("siteurl", siteurl);
			modelAndView.addObject("sitename", "openyelp");
			FrontUtils.format(request, modelAndView);
		}

	}
}
