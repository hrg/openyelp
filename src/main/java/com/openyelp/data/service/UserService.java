package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.UserRole;
import com.openyelp.data.entity.User;

public interface UserService {
	/**加密方法*/
	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;	//盐长度
	public Pagination getPage(int pageNo, int pageSize);

	public User findById(Long id);

	public User save(User bean);

	public User addRole(Long id, UserRole bean);

	public User update(User bean);

	public User deleteById(Long id);

	public User login(String username, String password, String macaddress);

	public User[] deleteByIds(Long[] ids);

	public User findByUsername(String username);
	
	public boolean checkPassword(User user,String oldPassword);

	public User updateUserLogin(User user);
}