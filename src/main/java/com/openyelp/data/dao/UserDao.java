package com.openyelp.data.dao;


import com.openyelp.data.core.BaseDao;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.entity.User;

public interface UserDao extends BaseDao<User, Long>{
	public Pagination getPage(int pageNo, int pageSize);

	public User findById(Long id);

	public User save(User bean);

	public User updateByUpdater(Updater<User> updater);

	public User deleteById(Long id);
}