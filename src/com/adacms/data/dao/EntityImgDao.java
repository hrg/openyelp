package com.adacms.data.dao;


import  com.adacms.data.core.BaseDao;
import  com.adacms.data.core.Updater;
import  com.adacms.common.Pagination;
import  com.adacms.data.entity.EntityImg;

public interface EntityImgDao extends BaseDao<EntityImg, Integer>{
	public Pagination getPage(int pageNo, int pageSize);

	public EntityImg findById(Integer id);

	public EntityImg save(EntityImg bean);

	public EntityImg updateByUpdater(Updater<EntityImg> updater);

	public EntityImg deleteById(Integer id);
}